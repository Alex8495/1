package Vimpelcom;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main extends Configured implements Tool {
  private static Logger logger = LoggerFactory.getLogger(Main.class);

  //example for input arguments
  //in/transactions.csv in/users.csv out/
  public static void main(final String[] args) throws Exception {
    BasicConfigurator.configure();
    ToolRunner.run(new Configuration(), new Main(), args);
  }


  public final int run(String[] args) throws Exception {
    if (args.length != 3) {
      System.err.printf("Usage: %s needs three arguments, transactions, users and output files\n", getClass().getSimpleName());
      return -1;
    }
    Configuration conf = getConf();
    conf.set("mapred.textoutputformat.separator", ":");

    Job job = Job.getInstance(conf, "SumValues");
    job.setJarByClass(Main.class);
    job.setMapperClass(MapClass.class);
    job.setReducerClass(ReduceClass.class);
    job.setMapOutputKeyClass(Text.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    job.setNumReduceTasks(1);

    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileInputFormat.addInputPath(job, new Path(args[1]));
    FileOutputFormat.setOutputPath(job, new Path(args[2]));

    job.waitForCompletion(true);

    if(job.isSuccessful()) {
      System.out.println("Job was successful");
    } else if(!job.isSuccessful()) {
      System.out.println("Job was not successful");
    }

    return 0;
  }

  public static class MapClass
          extends Mapper<Object, Text, Text, Text> {

    private Text value = new Text("");
    private Text ctn = new Text("");

    public void map(final Object key, final Text value, final Context context
    ) throws IOException, InterruptedException {
      String[] csv = value.toString().split(":");

      //skip first line
      if (csv[0].equals("ctn"))
        return;

      //if transactions.csv file
      if (csv.length == 3) {

        //get ctn, month and sum
        ctn.set(csv[0]);
        value.set(csv[1] + ":" + csv[2].replace(",", "."));
      }

      //if users.csv file
      else {

        //get ctn and nationality
        ctn.set(csv[0]);
        value.set(csv[1]);
      }
      context.write(ctn, value);
    }
  }

  public static class ReduceClass
          extends Reducer<Text, Text, Text, Text> {
    private Text result = new Text("");

    public void reduce(Text key, Iterable<Text> values,
                       Context context
    ) throws IOException, InterruptedException {
      HashMap<String, Double> hm = new HashMap<>();
      String nationality = "";
      for (Text val : values) {
        String[] spl = val.toString().split(":");

        //if value came from transactions.csv file
        if (spl.length == 2) {

          //add sum for month
          if (hm.containsKey(spl[0])) {
            hm.put(spl[0], hm.get(spl[0]) + Double.parseDouble(spl[1]));
          }

          //or update it
          else {
            hm.put(spl[0], Double.parseDouble(spl[1]));
          }
        }

        //if value came from users.csv file
        else {

          //remember nationality
          nationality = spl[0];
        }
      }

      //write all months with sum of transactions and nationality
      for (Map.Entry me : hm.entrySet()) {
        result.set (me.getKey() + ":" + me.getValue() + ":" + nationality);
        context.write(key, result);
      }
    }
  }
}
